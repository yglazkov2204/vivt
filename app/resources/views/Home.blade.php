<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght.." rel="stylesheet">
</head>

<body>

    <div class="form">
        <div class="form__container">
            <div class="form__item form__item-l">

            </div>
            <div class="form__item form__item-r">
                <h1>Регистрация в системе</h1>
                <p class="form__desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>

                <form action="" id="form__reg">
                    <label for="form__input_name">Ваше имя</label>
                    <input type="text" name="form__input_name" class="form__input" id="form__input_name"
                        placeholder="Юрий Глазков">
                    <br /><br />
                    <label for="form__input_email">Ваш E-mail</label>
                    <input type="text" name="form__input_email" class="form__input" id="form__input_email"
                        placeholder="example@mail.ru">
                    <br /><br />
                    <div class="form__wrapper">
                        <button type="submit" name="form__btn" class="form__btn" id="form__input_btn">Далее</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>

<style>
* {
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
}
.form {
    width: 100%;
    min-height: 100vh;
    display: flex;
}
.form__container {
    margin: auto;
    display: flex;
    border-radius: 10px;
    -webkit-box-shadow: -2px 0px 50px 10px #10101033;
    -moz-box-shadow: -2px 0px 50px 10px #10101033;
    box-shadow: -2px 0px 50px 10px #10101033;
    overflow: hidden;
}
.form__item {
    min-height: 20vw;
}
.form__item-l {
    padding: 1vw;
    width: 15vw;
    background-color: #f0f0f0;
}
.form__item-r {
    padding: 1vw;
    width: 30vw;
    background-color: #fff;
}
.form h1 {
    width: 90%;
    font-size: 2vw;
    font-weight: 700;
    color: #101010;
}
.form__desc {
    margin-top: .5vw;
    width: 90%;
    font-size: .9vw;
    font-weight: 500;
    color: #101010da;
}
#form__reg {
    margin: 2vw 0;
}
#form__reg label {
    margin-bottom: .2vw;
    font-size: .8vw;
    font-weight: 500;
    color: #10101090;
    display: block;
}
.form__input {
    outline: none;
}
.form__input {
    padding: 0 .5vw;
    width: calc(90% - 1vw);
    height: 2.2vw;
    border: 1px solid #6530f8;
    border-radius: 4px;
    transition: all 1s;
}
.form__input:focus {
    border: 1px solid #101010;
    -webkit-box-shadow: 0px 0px 20px -1px #10101033;
    -moz-box-shadow: 0px 0px 20px -1px #10101033;
    box-shadow: 0px 0px 20px -1px #10101033;
    transition: all 1s;
}
.form__wrapper {
    width: 90%;
    display: flex;
    justify-content: flex-end;
}
.form__btn {
    margin: 0;
    margin-top: 1rem;
    padding: .5vw 2vw;
    width: auto;
    height: auto;
    background-color: #6530f8;
    border: 1px solid #6530f8;
    border-radius: 5px;
    text-align: right;
    color: #fff;
    transition: all 1s;
}
.form__btn:hover {
    padding: .5vw 0;
    padding-left: 4vw;
    cursor: pointer;
    background-color: unset;
    border: 1px solid #6530f800;
    color: #6530f8;
    transition: all 1s;
}