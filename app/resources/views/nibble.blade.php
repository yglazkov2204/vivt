<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
</head>
<body>
    <header class="header">
        <div class="header__wrapper">
            <img src="https://nibbleteam.ru/storage/app/media/LogoNibble800.png" alt="" width="50">
            <p class="header__desc">Двухэтапная аутентификация</p>
            <div class="links">
                <a href="{{ route('login') }}" class="links__item">Войти</a>
                <a href="{{ route('register') }}" class="links__item">Зарегистрироваться</a>
            </div>
        </div>
    </header>
    
        <section class="section">
            <div class="section__desc">
                <h2>Ещё более безопасный аккаунт!</h2>
                <p>Защитите свой аккаунт от взлома с помощью мобильного телефона.</p>
            </div>
            <img class="section__img" src="https://te-st.ru/wp-content/uploads/2019/08/shutterstock_1260400450.jpg" alt="">
        </section>

        <h2 class="how-its-work__title">Как это работает?</h2>
        <div class="how-its-work">
            <div class="desc__wrapper">
                <h3>Уникальные коды для входа в аккаунт</h3>
                <p>Для входа в аккаунт используются уникальные одноразовые коды. Их можно получать по SMS, с помощью голосового вызова или приложения, установленного на телефоне.</p>
            </div>
            <img class="how-its-work__img" src="https://gamiha.net/uploads/e1bde13b4a4343a6ab3308848aa414ac.jpg" alt="">
        </div>

        <h2 class="enter__title">Как входить в аккаунт?</h2>
        <div class="enter">
            <div class="enter__desc">
                <h3>Как изменится процедура входа в аккаунт</h3>
                <div class="steps">
                    <div class="first-step">
                        <h4>Ввод пароля</h4>
                        <p>Это действие вам уже знакомо. Вы вводите пароль каждый раз, когда входите в аккаунт Google.</p>
                    </div>
                    <div class="second-step">
                        <h4>Ввод кода подтверждения</h4>
                        <p>Теперь вам нужно ввести специальный код. Вы можете получить его по SMS, с помощью голосового вызова или приложения, установленного на телефоне. Код не понадобится, если у вас есть токен – просто вставьте его в USB-порт компьютера.</p>
                    </div>
                </div>  
            </div>
            <img src="https://www.google.ru/landing/2step/images/how-works-img-1.png" alt="">
        </div>
</body>
</html>

<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Open Sans', sans-serif;
}

.header {
    width: 100%;
    height: auto;
}

.header__wrapper {
    width: 80%;
    margin: 0 auto;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
}

.header__desc {
    font-size: 30px;
}

.links__item {
    margin: 10px;
    color: black;
    font-size: 20px;
}


.section {
    width: 800px;
    margin: 50px auto;
    display: flex;
    flex-direction: row;
    align-items: center;
}


.section__desc > h2 {
    width: 400px;
    font-size: 35px;
}

h2 {font-size: 35px;}

.section__desc > p {
    width: 400px;
    font-size: 20px;
}

.section__img {
    width: 400px;
}

.how-its-work {
    width: 800px;
    margin: 50px auto;
    display: flex;
    flex-direction: row;
    align-items: center;
}

.how-its-work__desc {
    width: 400px;
    display: flex;
    flex-direction: row;
} 

.how-its-work__title {
    width: 400px;
    margin: 0 auto;
    text-align: center;
}

.how-its-work__img {
    width: 400px;
}

.desc__wrapper > p {
    margin-top: 7px;
}

.enter {
    width: 800px;
    margin: 50px auto;
    display: flex;
    flex-direction: row;
    align-items: center;
}

.steps {
    display: flex;
    flex-direction: column;
}

.first-step,
.second-step {
    width: 400px;
    display: flex;
    flex-direction: column;
}

.enter__title {
    text-align: center;
}

</style>